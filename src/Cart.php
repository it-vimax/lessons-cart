<?php


namespace app;


class Cart
{
    private $items;

    public function getItems()
    {
        $this->loadItems();
        return $this->items;
    }

    public function add($id, $count)
    {
        $this->loadItems();
        $current = isset($this->items[$id]) ? $this->items[$id] : 0;
        $this->items[$id] = $current + $count;
        $_SESSION['cart'] = serialize($this->items);
    }

    public function remove($id)
    {
        $this->loadItems();
        if (array_key_exists($id, $this->items)) {
            unset($this->items[$id]);
        }
        $_SESSION['cart'] = serialize($this->items);
    }

    public function clear()
    {
        $this->items = [];
       $this->saveItems();
    }

    private function loadItems()
    {
        if ($this->items === null) {
            $this->items = isset($_SESSION['cart']) ? unserialize($_SESSION['cart']) : [];
        }
    }

    private function saveItems()
    {
        $_SESSION['cart'] = serialize($this->items);
    }
}
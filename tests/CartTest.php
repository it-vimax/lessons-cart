<?php


use app\Cart;

class CartTest extends PHPUnit\Framework\TestCase
{
    public function testCreate()
    {
        self::dropSession();

        $cart = new Cart();
        $this->assertEquals([], $cart->getItems());
    }

    public function testAdd()
    {
        self::dropSession();

        $cart = new Cart();
        $cart->add(5, 3);
        $this->assertEquals([5 => 3], $cart->getItems());
    }

    public function testAddExist()
    {
        self::dropSession();

        $cart = new Cart();
        $cart->add(5, 2);
        $cart->add(5, 5);
        $this->assertEquals([5 => 7], $cart->getItems());
    }

    public function testRemove()
    {
        self::dropSession();

        $cart = new Cart();
        $cart->add(5, 3);
        $cart->remove(5);
        $this->assertEquals([], $cart->getItems());
    }

    public function testClear()
    {
        self::dropSession();

        $cart = new Cart();
        $cart->add(5, 3);
        $cart->clear();
        $this->assertEquals([], $cart->getItems());
    }

    private static function dropSession()
    {
        unset($_SESSION['cart']);;
    }
}